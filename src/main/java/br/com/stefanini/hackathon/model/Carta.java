package br.com.stefanini.hackathon.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Carta {

	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column
	private String mensagem;
	
	@Column(name= "nomeDestinatario")
	private String nomeDestinatario;
	
	@Column(name= "nomeRemetente")
	private String nomeRemetente;
	
	@Column(name= "EnderecoRemetente")
	private String enderecoRemetente;
	
	@Column(name= "enderecoDestinatario")
	private String enderecoDestinatario;
	
	@Column(name= "contatoDesti")
	private String contatoDesti;
	
	@Column(name= "contatoRemet")
	private String contatoRemet;
	
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}	

	public String getMensagem() {
		return mensagem;
	}
	
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getNomeDestinatario() {
		return nomeDestinatario;
	}

	public void setNomeDestinatario(String nomeDestinatario) {
		this.nomeDestinatario = nomeDestinatario;
	}

	public String getNomeRemetente() {
		return nomeRemetente;
	}

	public void setNomeRemetente(String nomeRemetente) {
		this.nomeRemetente = nomeRemetente;
	}

	public String getEnderecoRemetente() {
		return enderecoRemetente;
	}

	public void setEnderecoRemetente(String enderecoRemetente) {
		this.enderecoRemetente = enderecoRemetente;
	}

	public String getEnderecoDestinatario() {
		return enderecoDestinatario;
	}

	public void setEnderecoDestinatario(String enderecoDestinatario) {
		this.enderecoDestinatario = enderecoDestinatario;
	}

	public String getContatoDesti() {
		return contatoDesti;
	}

	public void setContatoDesti(String contatoDesti) {
		this.contatoDesti = contatoDesti;
	}

	public String getContatoRemet() {
		return contatoRemet;
	}

	public void setContatoRemet(String contatoRemet) {
		this.contatoRemet = contatoRemet;
	}



	
	
	
	
	
	
	
}
